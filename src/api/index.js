import axios from "axios";

export async function login(request) {
    return axios.post(`${process.env.REACT_APP_JSON_SERVER}/sign_in`, {
        request
    });
}

export async function getProducts() {
    return axios.get(`${process.env.REACT_APP_JSON_SERVER}/products`);
}
