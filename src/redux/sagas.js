import { all } from 'redux-saga/effects';
import loginSaga from "../features/login/sagas";
import productsSaga from "../features/products/sagas";

export default function* rootSaga(getState) {
  yield all([
    loginSaga(),
    productsSaga()
  ]);
}
