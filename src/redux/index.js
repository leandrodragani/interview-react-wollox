import { createStore, combineReducers, applyMiddleware, compose } from "redux";
import { createBrowserHistory } from "history";
import { routerMiddleware, connectRouter } from "connected-react-router";
import createSagaMiddleware from "redux-saga";
import { createLogger } from "redux-logger";
import reducers from "./reducers";
import rootSaga from "./sagas";
import { loadState, saveState } from "../localStorage";
import { throttle } from "lodash";

const history = createBrowserHistory();

const loggerMiddleware = createLogger({
  predicate: () => process.env.NODE_ENV === "development"
});

const sagaMiddleware = createSagaMiddleware();

const middlewares = [sagaMiddleware, loggerMiddleware, routerMiddleware(history)];

const store = createStore(
  combineReducers({
      router: connectRouter(history),
    ...reducers
  }),
  loadState(),
  compose(applyMiddleware(...middlewares)),
);

sagaMiddleware.run(rootSaga);

store.subscribe(throttle(() => {
  saveState(store.getState().login);
}, 1000));

export { store, history };
