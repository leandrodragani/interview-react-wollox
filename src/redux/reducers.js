import login from '../features/login/reducer';
import products from '../features/products/reducer';

export default {
  login,
  products
};
