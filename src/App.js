import React, { lazy, Suspense } from "react";
import {
  Route,
  Switch,
  Redirect
} from "react-router-dom";
import { Provider } from "react-redux";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { store, history } from "./redux";
import { ConnectedRouter } from 'connected-react-router';
import { Container, GlobalStyles } from "./styles";
import { NavBar, Footer, Spinner } from "./components";
import { Selectors } from "./features/login/selectors";

const Landing = lazy(() => import("./features/landing"));
const Login = lazy(() => import("./features/login"));
const Products = lazy(() => import("./features/products"));

toast.configure();

const ProtectedRoute = ({ component: Component, isLoggedIn, ...props }) => (
  <Route
    {...props}
    render={props =>
      isLoggedIn ? (
        <Component {...props} />
      ) : (
        <Redirect
          to={{
            pathname: "/login"
          }}
        />
      )
    }
  />
);

const isLoggedIn = store.getState().login.isLoggedIn;

console.log(isLoggedIn);

function App() {
  return (
    <div>
      <GlobalStyles />
      <Provider store={store}>
        <Container>
          <ConnectedRouter history={history}>
            <NavBar />
            <Suspense fallback={<Spinner />}>
              <Switch>
                <Route exact path="/" component={Landing} />
                <Route exact path="/login" component={Login} />
                <Route
                  exact
                  path="/products"
                  component={Products}
                />
              </Switch>
            </Suspense>
          </ConnectedRouter>
        </Container>
        <Footer />
      </Provider>
    </div>
  );
}

export default App;
