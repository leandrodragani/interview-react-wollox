import styled, { createGlobalStyle } from "styled-components";

export const GlobalStyles = createGlobalStyle`
  body {
    @import url('https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700&display=swap');
    font-family: 'Montserrat', sans-serif;
    height: 100%; 
  }

  html { 
      height: 100%; overflow:auto; 
      scroll-behavior: smooth;
  }
`

export const Container = styled.div`
    display: flex;
    max-width: 1350px;
	  margin: auto;
    flex-direction: column;
`