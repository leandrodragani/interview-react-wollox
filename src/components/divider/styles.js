import styled from "styled-components";

export const Hr = styled.hr`
    border: 0;
    height: 50px;
    margin-top: 0;
    box-shadow: 0 20px 20px -20px #333;
    margin: -50px auto 10px; 
`