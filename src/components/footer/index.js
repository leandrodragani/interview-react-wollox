import React from 'react';
import { FooterContainer, WooloxLogo  } from "./styles";

export const Footer = (props) => {
  return (
    <FooterContainer>
        <WooloxLogo src={require('../../assets/images/Ic_Wolox_Footer.svg')}/>
    </FooterContainer>
  );
};
