import styled from "styled-components";
import img from "../../assets/images/Backgrounds/Bg_Footer.png";

export const FooterContainer = styled.footer`
    display: flex;
    flex: 1 1 auto;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    padding: 10px;
    background-image: url(${img});
`

export const WooloxLogo = styled.img`
    width: 110px;
    height: 90px;
`