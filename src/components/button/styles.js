import styled from "styled-components";
import Colors from "../../utils/constants/colors";

export const StyledButton = styled.button`
  background: transparent;
  border-radius: 50px;
  border: 1.5px solid ${Colors.deepSkyBlue};
  margin: 5px;
  padding: 10px;
  background: ${props => props.primary ? Colors.deepSkyBlue : Colors.white};
  color: ${props => props.primary ? Colors.white : Colors.deepSkyBlue};
  &:hover {
    opacity: 0.5;
    cursor: pointer;
  }
`

export const ButtonText = styled.b`
  padding: 0px 60px;
  margin: 5px 0px;
  font-size: 15px;
`