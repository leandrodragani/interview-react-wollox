import React from 'react';
import { StyledButton, ButtonText } from "./styles";

export const Button = (props) => {
  const { onClick, text, primary } = props;
  return (
    <StyledButton primary={primary} onClick={onClick}>
        <ButtonText>
            {text}
        </ButtonText>
    </StyledButton>
  );
};
