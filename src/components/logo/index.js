import React from 'react';
import { WolloxLogo } from "./styles";

export const Logo = (props) => {
  const { image, onClick, width, height } = props;
  return (
    <WolloxLogo src={image} href={onClick} width={width} height={height} />
  );
};
