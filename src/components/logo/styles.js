import styled, { css } from "styled-components";

export const WolloxLogo = styled.img`
    ${props => props.width !== undefined && css`width: ${props.width}px`};
    ${props => props.height !== undefined && css`height: ${props.height}px`};
`