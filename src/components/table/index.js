import React from "react";
import { StyledTable, Row, HeaderColumn, Column } from "./styles";

export const Table = props => {
  const { columns, data } = props;
  return (
    <StyledTable>
      <thead>
        <Row>
          {columns.map(key => {
            return <HeaderColumn key={key}>{key}</HeaderColumn>;
          })}
        </Row>
      </thead>
      <tbody>
          {data.map(x => {
            return (
              <Row>
                {columns.map(key => {
                  return <Column key={x[key].id}>{x[key]}</Column>;
                })}
              </Row>
            );
          })}
      </tbody>
    </StyledTable>
  );
};
