import styled from "styled-components";
import Colors from "../../utils/constants/colors"; 

export const StyledTable = styled.table`
    width: 100%;
    background: #fff;
    color: black;
    font: inherit;
    box-shadow: 0 6px 10px 0 rgba(0, 0, 0 , .1);
    border: 0;
    outline: 0;
    padding: 22px 18px;
    margin: 4px 0px 25px 0px;
    tbody:before {
        content: "-";
        display: block;
        line-height: 0.6em;
        color: transparent;
    }
`

export const Row = styled.tr`
    line-height: 4em;
    &:hover {
        background-color: ${Colors.lightBackground} 
    }
`

export const HeaderColumn = styled.th`
    text-align: center;
`

export const Column = styled.td`
    text-align: center;
`