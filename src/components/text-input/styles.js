import styled from "styled-components";

export const StyledInput = styled.input`
    display: flex;
    flex: 1 1 auto;
    background: #fff;
    color: black;
    font: inherit;
    box-shadow: 0 6px 10px 0 rgba(0, 0, 0 , .1);
    border: 0;
    outline: 0;
    padding: 22px 18px;
    ::placeholder,
    ::-webkit-input-placeholder {
        color: #ccc;
    }
    :-ms-input-placeholder {
        color: #ccc;
    }
    margin: 4px 0px 25px 0px;
`

export const Label = styled.p`
    font-size: 13px;
    font-weight: bold;
    text-transform: uppercase;
`