import React from "react";
import { StyledInput, Label } from "./styles";

export const TextInput = props => {
  const { label } = props;
  return (
    <>
      <Label>{label}</Label>
      <StyledInput {...props} />
    </>
  );
};
