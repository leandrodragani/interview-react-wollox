import React from "react";
import { StyledSelect, Label } from "./styles";
import { v4 } from "uuid";

export const Select = props => {
  const { label, options, defaultValue } = props;
  return (
    <>
      <Label>{label}</Label>
      <StyledSelect {...props}>
        <option key="default" value="" disabled>{defaultValue}</option>
        {options.map(option => {
          return <option key={v4()} label={option.label}>{option.value || option}</option>;
        })}
      </StyledSelect>
    </>
  );
};
