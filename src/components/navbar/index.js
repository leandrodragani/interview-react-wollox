import React, { useState } from "react";
import { useTranslation } from 'react-i18next';
import { push } from "connected-react-router";
import { store } from "../../redux";
import { Logo, Button, Select } from "../";
import {
  MenuContainer,
  NavItem,
  LogoContainer,
  ItemsContainer,
  ButtonContainer,
  LanguageContainer,
  StyledHashLink,
  StyledLink
} from "./styles";

export const NavBar = (props) => {
  const { t, i18n } = useTranslation();
  const language = useLanguageSelector('', i18n)

  return (
    <header>
      <MenuContainer>
        <LogoContainer>
          <Logo
            image={require("../../assets/images/logo_full_color.svg")}
            width={220}
            height={120}
          />
        </LogoContainer>
        <ItemsContainer>
          <NavItem>
            <StyledLink to="/">{t('navbar.home_link')}</StyledLink>
          </NavItem>
          <NavItem>
            <StyledHashLink to="/#tecnologias">{t('navbar.tec_link')}</StyledHashLink>
          </NavItem>
          <NavItem>
            <StyledHashLink to="/#beneficios">{t('navbar.benef_link')}</StyledHashLink>
          </NavItem>
          <NavItem>
            <StyledHashLink to="/#requerimientos">{t('navbar.req_link')}</StyledHashLink>
          </NavItem>
        </ItemsContainer>
        <ButtonContainer>
          <Button onClick={() => store.dispatch(push('/login'))} text="Login" />
        </ButtonContainer>
        <LanguageContainer>
            <Select
              defaultValue={t('language.placeholder')}
              options={[
                {
                  label: t('language.es'),
                  value: 'es'
                },
                {
                  label: t('language.en'),
                  value: 'en'
                },
              ]}
              value={language.value}
              onChange={language.onChange}
            />
        </LanguageContainer>
      </MenuContainer>
    </header>
  );
};

function useLanguageSelector(initialValue, i18n) {
  const [value, setValue] = useState(initialValue);

  function handleChange(event) {
    setValue(event.target.value);
    i18n.changeLanguage(event.target.value);
  };

  return {
    value,
    onChange: handleChange,
  };
}
