import styled from "styled-components";
import Colors from "../../utils/constants/colors";
import { HashLink } from "react-router-hash-link";
import { Link } from "react-router-dom";

export const MenuContainer = styled.nav`
    display: flex;
    flex: 1 1 auto;
    align-items: center;
    padding: 10px;
`
export const LogoContainer = styled.div`
    display: flex;
    flex-basis: 20%;
`

export const ItemsContainer = styled.ul`
    display: flex;
    align-items: flex-end;
    justify-content: space-around;
    flex-basis: 50%;
`

export const NavItem = styled.li`
    font-size: 22px;
    color: ${Colors.black};
    flex-basis: 10%;
    font-weight: bold;
    list-style-type: none;
`

export const ButtonContainer = styled.div`
    padding: 15px;
    flex-basis: 10%;
`

export const LanguageContainer = styled.div`
    padding: 15px;
    flex-basis: 10%;
`

export const StyledLink = styled(Link)`
    text-decoration: none;
    color: black;
    &:focus, &:hover, &:visited, &:link, &:active {
        text-decoration: none;
    }
`;

export const StyledHashLink = styled(HashLink)`
    text-decoration: none;
    color: black;
    &:focus, &:hover, &:visited, &:link, &:active {
        text-decoration: none;
    }
`;