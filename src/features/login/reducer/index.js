import { ActionsTypes } from '../actions';

const initialState = {
  isFetching: false,
  user: {},
  isLoggedIn: false,
};

export default function login(state = initialState, action = {}) {
  switch (action.type) {
    case ActionsTypes.USER_LOGIN_REQUEST:
      return {
        ...state,
        isFetching: true
      };
    case ActionsTypes.USER_LOGIN_RESPONSE:
      return {
        ...state,
        isFetching: false,
        user: action.response.data !== undefined ? action.response.data.request : {},
        isLoggedIn: !action.response.error
      };
    default:
      return state;
  }
}
