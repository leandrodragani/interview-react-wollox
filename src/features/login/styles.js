import styled from "styled-components";
import Colors from "../../utils/constants/colors";

export const Container = styled.div`
    display: flex;  
    flex-direction: column; 
    align-items: center;
    justify-content: center;
    padding: 50px;
`

export const Title = styled.p`
    font-size: 22px;
    font-weight: 600;
    color: ${Colors.deepSkyBlue};
`

export const Box = styled.form`
    background: ${Colors.lightBackground};
    border-radius: 5px;
    box-shadow: 0 1.5px 0 0 rgba(0,0,0,0.1);
    width: 450px;
    display: flex;  
    flex-direction: column;
    padding: 50px;
`

export const ButtonContainer = styled.div`
    display: flex;
    padding: 25px;
    flex-direction: column;
    align-items: center;
`