import { makeActionCreator } from '../../../utils/helpers/redux';

const USER_LOGIN_REQUEST = 'login/USER_LOGIN_REQUEST';
const USER_LOGIN_RESPONSE = 'login/USER_LOGIN_RESPONSE';

export const userLoginRequest = makeActionCreator(USER_LOGIN_REQUEST, 'request');
export const userLoginResponse = makeActionCreator(USER_LOGIN_RESPONSE, 'response');

export const ActionsTypes = {
  USER_LOGIN_REQUEST,
  USER_LOGIN_RESPONSE,
};

export const Actions = {
  userLoginRequest,
  userLoginResponse,
};
