const isFetchingSelector = state => state.login.isFetching;
const userSelector = state => state.login.user;
const isLoggedInSelector = state => state.login.isLoggedIn;

export const Selectors = {
  isFetchingSelector,
  userSelector,
  isLoggedInSelector
};