import React, { useState } from "react";
import { useTranslation } from "react-i18next";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { TextInput, Select, Button, Spinner } from "../../components";
import { Container, Box, ButtonContainer, Title } from "./styles";
import { range } from "lodash";
import { Actions } from "./actions";
import { Selectors } from "./selectors";

function Login(props) {
  const { actions, isFetching } = props;
  const { t } = useTranslation();

  const nombre = useInput("");
  const apellido = useInput("");
  const email = useInput("");
  const edad = useInput("");

  return (
    <Container>
      <Box>
        <Title>{t('login.title')}</Title>
        <TextInput
          label={t('login.input_label_name')}
          placeholder={t('login.input_placeholder_name')}
          value={nombre.value}
          onChange={nombre.onChange}
        />
        <TextInput
          label={t('login.input_label_surname')}
          placeholder={t('login.input_placeholder_surname')}
          value={apellido.value}
          onChange={apellido.onChange}
        />
        <TextInput
          label={t('login.input_label_email')}
          placeholder={t('login.input_placeholder_email')}
          value={email.value}
          onChange={email.onChange}
        />
        <Select
          label={t('login.input_label_age')}
          defaultValue={t('login.input_placeholder_age')}
          options={range(18, 66)}
          value={edad.value}
          onChange={edad.onChange}
        />
        <ButtonContainer>
          {isFetching ? (
            <Spinner />
          ) : (
            <Button
              primary
              onClick={e => {
                e.preventDefault();
                actions.userLoginRequest({
                  nombre: nombre.value,
                  apellido: apellido.value,
                  email: email.value,
                  edad: edad.value
                });
              }}
              text={t('login.button')}
            />
          )}
        </ButtonContainer>
      </Box>
    </Container>
  );
}

function useInput(initialValue) {
  const [value, setValue] = useState(initialValue);

  function handleChange(event) {
    setValue(event.target.value);
  };

  return {
    value,
    onChange: handleChange
  };
}

function mapStateToProps(state) {
  return {
    isFetching: Selectors.isFetchingSelector(state)
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({ ...Actions }, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Login);
