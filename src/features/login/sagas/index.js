import { call, put, takeEvery, delay } from 'redux-saga/effects';
import { push } from "connected-react-router";
import { toast } from 'react-toastify';
import { validate } from 'validate.js';
import { validations } from "../validations";
import { ActionsTypes, Actions } from '../actions';
import { login } from '../../../api';

function* fetchUserWorker(action) {
  try {
    const val = validate(action.request, validations, { format: 'flat' });
    let response;
    // Para mostrar el spinner
    yield delay(1000);
    
    if (val === undefined) {
      response = yield call(login, action.request);
      response.error = false;
      toast.success("Gracias por registrarte!");
    } else {
      response = { error: true };
      toast.error(val[0]);
    }
    if (!response.error) {
      yield put(push('/products'));
    }
    yield put(Actions.userLoginResponse(response));
  } catch (e) {
    toast.error("Ocurrio un error al intentar registrarse. Intente de nuevo.");
  }
}

function* loginSaga() {
  yield takeEvery(ActionsTypes.USER_LOGIN_REQUEST, fetchUserWorker);
}

export default loginSaga;
