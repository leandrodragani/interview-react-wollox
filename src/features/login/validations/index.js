export const validations = {
  nombre: {
    length: {
      minimum: 1,
      message: "^Para continuar debes completar el campo nombre."
    }
  },
  apellido: {
    length: {
      minimum: 1,
      message: "^Para continuar debes completar el campo apellido."
    }
  },
  email: {
    length: {
      minimum: 1,
      message: "^Para continuar debes completar el campo email."
    },
    email: {
      message: "^Ingrese un email valido."
    }
  },
  edad: {
    length: {
      minimum: 1,
      message: "^Para continuar debes completar el campo edad."
    }
  }
};
