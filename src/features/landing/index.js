import React, { Component } from "react";
import { Inicio } from "./components/inicio";
import { Tecnologias } from "./components/tecnologias";
import { Woloxers } from "./components/woloxers";
import { Beneficios } from "./components/beneficios";
import { Requerimientos } from "./components/requerimientos";
import { ConocerMas } from "./components/conocer-mas";
import { Divider } from "../../components";

export default class Landing extends Component {
  constructor() {
    super();
    this.state = {};
  }

  render() {
    return (
      <div>
        <Inicio />
        <Tecnologias />
        <Woloxers />
        <Beneficios
          beneficios={[
            {
              label: "landing.benefits_1",
              icon: require("../../assets/images/Ic_Hour.svg")
            },
            {
              label: "landing.benefits_2",
              icon: require("../../assets/images/Ic_HomeOffice.svg")
            },
            {
              label: "landing.benefits_3",
              icon: require("../../assets/images/Ic_Workshops.svg")
            },
            {
              label: "landing.benefits_4",
              icon: require("../../assets/images/Ic_DrinkSnacks.svg")
            },
            {
              label: "landing.benefits_5",
              icon: require("../../assets/images/Ic_laptop.svg")
            },
            {
              label: "landing.benefits_6",
              icon: require("../../assets/images/Ic_brain.svg")
            }
          ]}
        />
        <Divider />
        <Requerimientos
          requerimientos={[
            {
              bullet: require("../../assets/images/Ic_Bullet_1.svg"),
              label: "landing.requirements_1"
            },
            {
              bullet: require("../../assets/images/Ic_Bullet_2.svg"),
              label: "landing.requirements_2"
            },
            {
              bullet: require("../../assets/images/Ic_Bullet_3.svg"),
              label: "landing.requirements_3"
            }
          ]}
        />
        <ConocerMas />
      </div>
    );
  }
}
