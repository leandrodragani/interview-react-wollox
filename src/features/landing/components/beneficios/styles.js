import styled from "styled-components";

export const Container = styled.div`
    display: flex;
    flex: 1 1 auto;
    align-items: center;
    justify-content: center;
    padding: 35px 0px;
    flex-direction: column;
`

export const TitleText = styled.p`
    font-size: 38px;
    font-weight: 500;
`

export const BeneficiosContainer = styled.div`
    display: flex;
    justify-content: space-between;
`

export const BeneficioContainer = styled.div`
    display: flex;
    flex-basis: 16%;
    align-items: center;
    justify-content: center;
    flex-direction: column;
`

export const BeneficioIcon = styled.img`
    width: 100px;
    height: 100px;
`

export const BeneficioLabel = styled.p`
    font-size: 26px;
    text-align: center;
    font-weight: 500;
`