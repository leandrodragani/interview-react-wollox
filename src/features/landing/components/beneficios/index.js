import React from 'react';
import { BeneficiosContainer, TitleText, BeneficioContainer, BeneficioIcon, BeneficioLabel, Container } from "./styles";
import { v4 } from "uuid";
import { useTranslation } from "react-i18next";

export const Beneficios = (props) => {
    const { beneficios } = props;
    const { t } = useTranslation();

    return (
        <Container id="beneficios">
            <TitleText>{t('landing.benefits')}</TitleText>
            <BeneficiosContainer>
                {beneficios.map(beneficio => {
                    return (
                        <BeneficioContainer key={v4()}>
                            <BeneficioIcon src={beneficio.icon} />
                            <BeneficioLabel>{t(beneficio.label)}</BeneficioLabel>
                        </BeneficioContainer>
                    );
                })}
            </BeneficiosContainer>
        </Container>
    );
}
