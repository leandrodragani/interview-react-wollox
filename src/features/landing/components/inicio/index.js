import React from "react";
import { useTranslation } from "react-i18next";
import { Container, VectorHero, TitleText } from "./styles";
import Colors from "../../../../utils/constants/colors";

export const Inicio = () => {
  const { t } = useTranslation();

  return (
    <Container>
      <TitleText>
        {t('landing.inicio_title_1')} <br />
        <TitleText fontWeight="600">{t('landing.inicio_title_2')}</TitleText> {t('landing.inicio_title_3')} <br />
        <TitleText fontWeight="700" color={Colors.limeGreen}>
        {t('landing.inicio_title_4')}
        </TitleText>
      </TitleText>
      <VectorHero
        src={require("../../../../assets/images/ImgHero/Ic_ilustra_Hero.png")}
      />
    </Container>
  );
};
