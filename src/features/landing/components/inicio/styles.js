import styled, { css } from "styled-components";
import bg_header from "../../../../assets/images/Backgrounds/Bg_Header.png";

export const Container = styled.div`
    display: flex;
    flex: 1 1 auto;
    align-items: center;
    padding: 35px 0px;
    background-size: cover;
    background-image: url(${bg_header});
`

export const TitleText = styled.span`
    font-size: 48px;
    flex-basis: 40%;
    margin: 0;
    ${props => props.color !== undefined && css`color: ${props.color};`};
    ${props => props.fontWeight !== undefined && css`font-weight: ${props.fontWeight};`};
`

export const VectorHero = styled.img`
    margin-left: auto;
    margin-right: 40px;
`