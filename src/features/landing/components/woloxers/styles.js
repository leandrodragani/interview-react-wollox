import styled, { css } from "styled-components";
import img_woloxer from '../../../../assets/images/img_woloxer.png';
import Colors from "../../../../utils/constants/colors";

export const Container = styled.div`
    display: flex;
    flex: 1 1 auto;
    align-items: center;
    margin: 150px 0px;
`

export const VectorContainer = styled.div`
    display: flex;
    flex-basis: 55%;
    align-items:center;
    justify-content: center;
    flex-direction: column;
    background-image: url(${img_woloxer});
    height: 422px;
`

export const TitleContainer = styled.div`
    flex-basis: 50%;
    align-items:center;
    justify-content: center;
    display: flex;
    flex-direction: column;
    height: 422px;
    background-color: ${Colors.lightBackground}
`

export const LeftTitleText = styled.span`
    font-size: 48px;
    text-align: center;
    margin: 0;
    ${props => props.color !== undefined && css`color: ${props.color};`};
    ${props => props.fontWeight !== undefined && css`font-weight: ${props.fontWeight};`};
    text-align: center;
`

export const RightTitleText = styled.span`
    font-size: 50px;
    text-align: center;
    margin: 0;
    ${props => props.color !== undefined && css`color: ${props.color};`};
    ${props => props.fontWeight !== undefined && css`font-weight: ${props.fontWeight};`};
    text-align: center;
`
export const RightSubtitleText = styled.span`
    font-size: 20px;
    text-align: center;
    margin: 0;
    ${props => props.color !== undefined && css`color: ${props.color};`};
    ${props => props.fontWeight !== undefined && css`font-weight: ${props.fontWeight};`};
    text-align: center;
    padding: 25px;
`