import React from "react";
import { useTranslation } from "react-i18next";
import { Button } from "../../../../components";
import { Container, VectorContainer, LeftTitleText, TitleContainer, RightTitleText, RightSubtitleText } from "./styles";
import Colors from "../../../../utils/constants/colors";

export const Woloxers = () => {
  const { t } = useTranslation();

  return (
    <Container>
      <VectorContainer>
      <RightTitleText>
        <RightTitleText fontWeight="900" color={Colors.limeGreen}>350 + </RightTitleText>
        <RightTitleText fontWeight="900" color={Colors.deepSkyBlue}>Woloxers</RightTitleText>
      </RightTitleText>
        <RightSubtitleText fontWeight="normal" color={Colors.white}>@Wolox</RightSubtitleText>
        <Button
          primary
          onClick={() => console.log("follow")}
          text={t('landing.woloxers_button')}
        />
      </VectorContainer>
      <TitleContainer>
        <LeftTitleText>
          {t('landing.woloxers_1')} <br />
          <LeftTitleText fontWeight="700" color={Colors.limeGreen}>{t('landing.woloxers_2')}</LeftTitleText>
          <LeftTitleText fontWeight="700" color={Colors.deepSkyBlue}> {t('landing.woloxers_3')}</LeftTitleText> {t('landing.woloxers_4')} <br />
          {t('landing.woloxers_5')}.
        </LeftTitleText>
      </TitleContainer>
    </Container>
  );
};
