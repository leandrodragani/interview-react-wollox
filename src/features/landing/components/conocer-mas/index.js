import React from "react";
import { useTranslation } from "react-i18next";
import { TitleText, Container, SubtitleText, ButtonContainer } from "./styles";
import { Button } from "../../../../components";
import Colors from "../../../../utils/constants/colors";

export const ConocerMas = props => {
  const { t } = useTranslation();

  return (
    <Container>
      <TitleText>
        {t("landing.conocer_mas_title_1")}{" "}
        <TitleText color={Colors.deepSkyBlue}>
          {t("landing.conocer_mas_title_2")}
        </TitleText>
      </TitleText>
      <SubtitleText>{t("landing.conocer_mas_subtitle")}</SubtitleText>
      <ButtonContainer>
        <Button
          primary
          onClick={() => console.log("goto")}
          text={t("landing.conocer_mas_button")}
        />
      </ButtonContainer>
    </Container>
  );
};
