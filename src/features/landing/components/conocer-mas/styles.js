import styled, { css } from "styled-components";
import bg_footer from "../../../../assets/images/Backgrounds/Bg_Footer.png";

export const Container = styled.div`
    display: flex;
    flex: 1 1 auto;
    align-items: center;
    justify-content: center;
    padding: 100px;
    flex-direction: column;
    background-size: cover;
    background-image: url(${bg_footer});
`

export const TitleText = styled.span`
    font-size: 48px;
    margin: 4px 0px;
    font-weight: 600;
    ${props => props.color !== undefined && css`color: ${props.color};`};
`

export const SubtitleText = styled.p`
    font-size: 36px;
    margin: 4px 0px;
    font-weight: 500;
`

export const ButtonContainer = styled.div`
    padding: 50px;
`