import styled from "styled-components";

export const Container = styled.div`
    display: flex;
    flex: 1 1 auto;
    align-items: center;
    justify-content: space-between;
    padding: 100px;
`

export const TitleText = styled.p`
    flex-basis: 30%;
    font-size: 38px;
    text-align: right;
    font-weight: 900;
`

export const RequerimientosContainer = styled.div`
    display: flex;
    flex-basis: 50%;
    flex-direction: column;
`
    
export const ItemReqContainer = styled.div`
    display: flex;
    align-items: center;
`

export const ItemReqDot = styled.img`
`

export const ItemReqLabel = styled.p`
    font-size: 22px;
    margin-left: 20px;
    font-weight: 500;
`