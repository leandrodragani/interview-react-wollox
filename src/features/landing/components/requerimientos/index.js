import React from "react";
import { useTranslation } from "react-i18next";
import {
  TitleText,
  Container,
  RequerimientosContainer,
  ItemReqContainer,
  ItemReqDot,
  ItemReqLabel
} from "./styles";
import { v4 } from "uuid";

export const Requerimientos = props => {
  const { requerimientos } = props;
  const { t } = useTranslation();
  return (
    <Container id="requerimientos">
      <TitleText>{t('landing.requirements')}</TitleText>
      <RequerimientosContainer>
          {requerimientos.map(requerimiento => {
              return (
                <ItemReqContainer key={v4()}>
                    <ItemReqDot src={requerimiento.bullet}/>
                    <ItemReqLabel>{t(requerimiento.label)}</ItemReqLabel>
                </ItemReqContainer>
              );
          })}
      </RequerimientosContainer>
    </Container>
  );
};
