import React from 'react';
import { useTranslation } from "react-i18next";
import { Container, VectorTecnologias, TitleText } from "./styles";

export const Tecnologias = () => {
  const { t } = useTranslation();

  return (
    <Container id="tecnologias">
        <TitleText>{t('landing.technologies')}</TitleText>
        <VectorTecnologias src={require('../../../../assets/images/Ic_Tecnologys.svg')} />
    </Container>
  );
};
