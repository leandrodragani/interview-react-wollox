import styled from "styled-components";

export const Container = styled.div`
    display: flex;
    flex: 1 1 auto;
    flex-direction: row;
    flex-wrap: wrap;
    align-items: center;
    padding: 35px 0px;
`

export const TitleText = styled.p`
    font-size: 36px;
    flex-basis: 30%;
    font-weight: 500;
`

export const VectorTecnologias = styled.img`
    margin-left: auto;
    margin-right: 30px;
`