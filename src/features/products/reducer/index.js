import { ActionsTypes } from '../actions';

const initialState = {
  isFetching: false,
  products: [],
  filter: ""
};

export default function products(state = initialState, action = {}) {
  switch (action.type) {
    case ActionsTypes.PRODUCTS_REQUEST:
      return {
        ...state,
        isFetching: true
      };
    case ActionsTypes.PRODUCTS_RESPONSE:
      return {
        ...state,
        isFetching: false,
        products: action.response.data,
      };
    case ActionsTypes.SET_FILTER:
      return {
        ...state,
        filter: action.filter,
      };
    default:
      return state;
  }
}
