import { call, put, takeEvery, delay } from 'redux-saga/effects';
import { toast } from 'react-toastify';
import { ActionsTypes, Actions } from '../actions';
import { getProducts } from '../../../api';

function* fetchProductsWorker(action) {
  try {
    const response = yield call(getProducts, action.request);
    // Para mostrar el spinner
    yield delay(1000);
    yield put(Actions.productsResponse(response));
  } catch (e) {
    toast.error("Ocurrio un error al intentar obtener los productos. Intente de nuevo.");
  }
}

function* productsSaga() {
  yield takeEvery(ActionsTypes.PRODUCTS_REQUEST, fetchProductsWorker);
}

export default productsSaga;
