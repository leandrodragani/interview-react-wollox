import styled from "styled-components";
import Colors from "../../utils/constants/colors";

export const Title = styled.h1`
    color: ${Colors.deepSkyBlue};
`