import { createSelector } from "reselect";

const isFetchingSelector = state => state.products.isFetching;
const productsSelector = state => state.products.products;
const filterSelector = state => state.products.filter;
// Ya que habia que filtrar los datos a mano preferi realizar un memoized selector antes que volver a hacer el GET
const filterProductsSelector = createSelector(
  [filterSelector, productsSelector],
  (filterValue, products) =>
    filterValue.length > 0
      ? products.filter(product => product.name.toLowerCase().includes(filterValue.toLowerCase()))
      : products
);

export const Selectors = {
  isFetchingSelector,
  productsSelector,
  filterSelector,
  filterProductsSelector
};
