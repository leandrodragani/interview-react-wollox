import React, { useState, useEffect } from "react";
import { useTranslation } from "react-i18next";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { TextInput, Spinner, Table } from "../../components";
import { Title } from "./styles";
import { Actions } from "./actions";
import { Selectors } from "./selectors";

const withSpinner = Component => ({ isFetching, ...props }) => {
  if (isFetching) {
    return <Spinner />
  } else {
    return (
      <Component {...props} />
    )
  }
};

const TableWithSpinner = withSpinner(Table);

function Products(props) {
  const { actions, products, isFetching } = props;
  const filter = useFilter("", actions.setFilter);
  const { t } = useTranslation();

  useEffect(() => {
    actions.productsRequest();
  }, []);

  return (
    <>
      <Title>{t('products.title')}</Title>
      <TextInput placeholder={t('products.filter_placeholder')} value={filter.value} onChange={filter.onChange}/>
      <div>
        <TableWithSpinner
          isFetching={isFetching}
          columns={["id", "sku", "name", "price"]}
          data={products}
        />
      </div>
    </>
  );
}

function useFilter(initialValue, callback) {
  const [value, setValue] = useState(initialValue);

  function handleChange(event) {
    setValue(event.target.value);
    callback(event.target.value);
  }

  return {
    value,
    onChange: handleChange
  };
}

function mapStateToProps(state) {
  return {
    isFetching: Selectors.isFetchingSelector(state),
    products: Selectors.filterProductsSelector(state)
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({ ...Actions }, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Products);
