import { makeActionCreator } from "../../../utils/helpers/redux";

const PRODUCTS_REQUEST = "products/PRODUCTS_REQUEST";
const PRODUCTS_RESPONSE = "products/PRODUCTS_RESPONSE";
const SET_FILTER = "products/SET_FILTER";

export const productsRequest = makeActionCreator(PRODUCTS_REQUEST, "request");
export const productsResponse = makeActionCreator(PRODUCTS_RESPONSE, "response");
export const setFilter = makeActionCreator(SET_FILTER, "filter");

export const ActionsTypes = {
  PRODUCTS_REQUEST,
  PRODUCTS_RESPONSE,
  SET_FILTER
};

export const Actions = {
  productsRequest,
  productsResponse,
  setFilter
};
