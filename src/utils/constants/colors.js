export default {
  black: 'black',
  deepSkyBlue: '#2AA7DF',
  gray: '#ccc',
  white: 'white',
  lightBackground: '#eee',
  limeGreen: '#A3CC39',
};